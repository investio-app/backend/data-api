# API - Investio

Run a dev server

```bash
$ go run main.go
```

To build a docker image

```bash
$ docker build -t registry.gitlab.com/investio-app/backend/c/public-api:dev . --build-arg version="dev-$(date +%s)"
$ docker push registry.gitlab.com/investio-app/backend/c/public-api:dev
```
