package main

import (
	"os"
	"regexp"
	"strings"
	_ "time/tzdata"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"github.com/gin-contrib/cors"
	"github.com/joho/godotenv"
	"gitlab.com/investio/backend/api/db"
	"gitlab.com/investio/backend/api/v1/controller"
	"gitlab.com/investio/backend/api/v1/service"
)

var (
	thaiRegEx, _ = regexp.Compile("([\u0E00-\u0E7F]+)")

	fundService    service.FundService    = service.NewFundService(thaiRegEx)
	navService     service.NavService     = service.NewNavService()
	statService    service.StatService    = service.NewStatService()
	predictService service.PredictService = service.NewPredictService()

	fundController    controller.FundController    = controller.NewFundController(fundService)
	navController     controller.NavController     = controller.NewNavController(navService)
	statController    controller.StatController    = controller.NewStatController(statService)
	predictController controller.PredictController = controller.NewPredictController(predictService)
)

func getVersion(ctx *gin.Context) {
	ver := os.Getenv("VERSION")
	ctx.JSON(200, gin.H{
		"version": ver,
	})
}

func setCors() (corsConfig cors.Config) {
	corsConfig = cors.DefaultConfig()
	origins := os.Getenv("ALLOW_ORIGINS")
	oriList := strings.Split(origins, ",")
	if oriList[0] == "" {
		oriList = []string{"*"}
	}
	corsConfig.AllowOrigins = oriList

	// corsConfig.AllowMethods = []string{"PUT"}
	corsConfig.AllowHeaders = []string{"Authorization", "content-type"}
	// To be able to send tokens to the server.
	corsConfig.AllowCredentials = true

	// OPTIONS method for VueJS
	corsConfig.AddAllowMethods("OPTIONS")
	return
}

func main() {
	var err error

	logLevel := os.Getenv("LOG_LEVEL")

	switch strings.ToLower(logLevel) {
	case "debug":
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	case "trace":
		zerolog.SetGlobalLevel(zerolog.TraceLevel)
	default:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	}

	log.Info().Str("version", os.Getenv("VERSION")).Msg("started")

	if os.Getenv("GIN_MODE") != "release" {
		err = godotenv.Load()
		if err != nil {
			log.Fatal().Err(err).Msg("Error loading .env file")
		}
	}

	err = db.SetupDB()
	if err != nil {
		log.Fatal().Err(err).Msg("DB connection issue")
	}

	defer db.InfluxClient.Close()

	server := gin.Default()

	server.Use(cors.New(setCors()))

	v1 := server.Group("/v1")
	{
		f := v1.Group("/funds")
		{
			f.GET("/info/:id", fundController.GetFundByID)
			f.GET("/cats", fundController.ListCat)
			f.GET("/amcs", fundController.ListAmc)
			f.GET("/nav/series", navController.GetPastNav)
			f.GET("/nav", navController.GetNavByDate)
			f.GET("/search/:fundQuery", fundController.SearchFund)
			f.GET("/top/return", statController.GetTopReturn)
			f.GET("/stat/:fundID", statController.GetStatInfo)

		}
		v1.GET("/predict", predictController.GetTopPredict)
		v1.POST("/predict", predictController.AddPredict)
	}

	internalV1 := server.Group("/s2gsoeq93f/v1")
	{
		internalV1.GET("/index/set", navController.GetPastSetIndex)
		internalV1.GET("/nav/series/ast", navController.GetPastNavWithAsset)
		internalV1.GET("/predict/scope", fundController.ListPredictScope)
	}

	v1.GET("/ver", getVersion)
	log.Info().Msg("listening...")
	server.Run(":5005")
}
